class Page {
  get searchInput() { return $('input[type="search"]') }
  get searchButton() { return $('button[data-role="search-button"]') }
  get acceptCookieButton()  { return $('button[data-role="accept-consent"]') }
  get itemList() { return $('#opbox-listing--base') }
  get firstItem() { return this.itemList.$('article[data-item="true"]') }
  get firstItemTitle() { return this.firstItem.$('h2 > a') }
  get itemSummary() { return $('div[data-box-name="summary"]') }
  get itemTitle() { return this.itemSummary.$('h1')}
  get itemPrice() { return this.itemSummary.$('meta[itemprop="price"]') }
  get itemPriceCurrency() { return this.itemSummary.$('meta[itemprop="priceCurrency"]') }

  closeCookieModal() {
    this.acceptCookieButton.click();
  }

  searchForProduct(item) {
    this.searchInput.keys(item);
    this.searchButton.click();
  }

  selectProduct() {
    this.itemList.waitForDisplayed();
    this.firstItemTitle.click();
  }

  getProductTitle() {
    this.itemSummary.waitForDisplayed();
    const productTitle = this.itemTitle.getText();
    return productTitle.trim();
  }

  getProductPrice() {
    this.itemSummary.waitForDisplayed();
    const price = this.itemPrice.getAttribute("content");
    return price;
  }

  getProductPriceCurrency() {
    this.itemSummary.waitForDisplayed();
    const priceCurrency = this.itemPriceCurrency.getAttribute("content");
    return priceCurrency;
  }
}

module.exports = new Page();