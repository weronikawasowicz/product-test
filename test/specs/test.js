const page = require('../pages/page.js');

const SEARCH_PRODUCT_VALUE = 'iphone';
const PRICE_CURRENCY = 'PLN';

describe('Check product', () => {
  before(function() {
    browser.url('');
    page.closeCookieModal();
    page.searchForProduct(SEARCH_PRODUCT_VALUE);
    page.selectProduct();
  });

  it('title is not empty', function() {
    const title = page.getProductTitle();
    chai.assert.isNotEmpty(title);
  });

  it('price in schema markup for SERP is not empty', function() {
    const price = page.getProductPrice();
    chai.assert.isNotEmpty(price);
  });

  it('price has proper currency in schema markup for SERP', function() {
    const priceCurrency = page.getProductPriceCurrency();
    expect(priceCurrency).toEqual(PRICE_CURRENCY);
  })
});